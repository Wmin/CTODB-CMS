//package cn.ctodb.freemarker.macro;
//
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//
//import javax.annotation.Resource;
//
//import org.springframework.stereotype.Service;
//import org.viva.service.cms.LinkService;
//
//import freemarker.core.Environment;
//import freemarker.template.ObjectWrapper;
//import freemarker.template.TemplateDirectiveBody;
//import freemarker.template.TemplateDirectiveModel;
//import freemarker.template.TemplateException;
//import freemarker.template.TemplateModel;
//
//@Service("cms_link_list")
//public class LinkDirective implements TemplateDirectiveModel {
//
//    @Resource
//    private LinkService linkService;
//
//    @SuppressWarnings({ "rawtypes", "deprecation", "unchecked" })
//    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
//        if (params.isEmpty()) params = new HashMap<>();
//        env.setVariable("links", ObjectWrapper.DEFAULT_WRAPPER.wrap(linkService.getLinks(params)));
//        body.render(env.getOut());
//    }
//
//}
