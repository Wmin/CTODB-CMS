package cn.ctodb.freemarker.macro;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cn.ctodb.service.UserService;
import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Service("cms_user_list")
public class UserDirective implements TemplateDirectiveModel {

    @Resource
    private UserService userService;

    @SuppressWarnings({ "rawtypes", "deprecation" })
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        if (params.isEmpty()) params = new HashMap<>();
        Pageable pageable = null;
        env.setVariable("users", ObjectWrapper.DEFAULT_WRAPPER.wrap(userService.list(pageable)));
        body.render(env.getOut());
    }

}
