//package cn.ctodb.freemarker.macro;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//
//import org.springframework.stereotype.Service;
//import org.viva.admin.util.page.Page;
//import org.viva.service.cms.MovieService;
//
//import freemarker.core.Environment;
//import freemarker.template.ObjectWrapper;
//import freemarker.template.TemplateDirectiveBody;
//import freemarker.template.TemplateDirectiveModel;
//import freemarker.template.TemplateException;
//import freemarker.template.TemplateModel;
//
//@Service("cms_movie_page")
//public class MovieListDirective implements TemplateDirectiveModel {
//
//    public static List<String> areas = new ArrayList<>();
//    static{
//        areas.add("美国");
//        areas.add("大陆");
//        areas.add("香港");
//        areas.add("台湾");
//        areas.add("韩国");
//        areas.add("日本");
//        areas.add("泰国");
//        areas.add("英国");
//    }
//    
//    @Resource
//    private MovieService movieService;
//
//    @SuppressWarnings({ "rawtypes", "deprecation", "unchecked" })
//    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
//        if (params.isEmpty()) params = new HashMap<>();
//        Page page = movieService.getPage(params);
//        env.setVariable("page", ObjectWrapper.DEFAULT_WRAPPER.wrap(page));
//        env.setVariable("areas", ObjectWrapper.DEFAULT_WRAPPER.wrap(areas));
//        body.render(env.getOut());
//    }
//
//}
