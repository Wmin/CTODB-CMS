package cn.ctodb.freemarker.macro;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cn.ctodb.service.TagService;
import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Service("cms_tag_list")
public class TagDirective implements TemplateDirectiveModel {

    @Resource
    private TagService tagService;

    @SuppressWarnings({ "rawtypes", "deprecation" })
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
            throws TemplateException, IOException {
        if (params.isEmpty()) params = new HashMap<>();
        Pageable pageable = null;
        env.setVariable("tags", ObjectWrapper.DEFAULT_WRAPPER.wrap(tagService.getTags(pageable)));
        body.render(env.getOut());
    }

}
