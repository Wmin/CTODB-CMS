package cn.ctodb.freemarker.macro;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import cn.ctodb.domain.User;
import cn.ctodb.service.SysConfService;
import cn.ctodb.service.UserService;
import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Component
public class MyDirective implements TemplateDirectiveModel {

    @Resource
    private UserService    userService;
    @Resource
    private SysConfService sysConfService;

    @SuppressWarnings({ "deprecation", "rawtypes" })
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
            throws TemplateException, IOException {
        User user = userService.getUserWithAuthorities();
        env.setVariable("my", ObjectWrapper.DEFAULT_WRAPPER.wrap(user));
        if (user == null) {
//            SysConf appid = sysConfService.getByCode("qq.conn.appid");
//            SysConf scope = sysConfService.getByCode("qq.conn.scope");
//            SysConf redirect_uri = sysConfService.getByCode("qq.conn.redirecturi");
            // env.setVariable("qqLoginUrl",
            // ObjectWrapper.DEFAULT_WRAPPER.wrap(ConnectQQ.getLoginUrl(appid,
            // scope, redirect_uri)));
        }
        body.render(env.getOut());
    }

}
