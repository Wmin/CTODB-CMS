package cn.ctodb.freemarker.macro;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cn.ctodb.domain.Content;
import cn.ctodb.service.ContentService;
import freemarker.core.Environment;
import freemarker.template.ObjectWrapper;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

@Service("cms_content_page")
public class ContentPageDirective implements TemplateDirectiveModel {

    @Resource
    private ContentService contentService;

    @SuppressWarnings({ "rawtypes", "deprecation" })
    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body)
            throws TemplateException, IOException {
        if (params.isEmpty()) params = new HashMap<>();
        Pageable pageable = null;
        Page<Content> page = contentService.getPage(pageable);
        env.setVariable("page", ObjectWrapper.DEFAULT_WRAPPER.wrap(page));
        body.render(env.getOut());
    }

}
