package cn.ctodb.app.mail;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.ctodb.domain.User;

@Entity
@Table(name = "t_mail_account")
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class MailAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Size(max = 10)
	@Column(length = 10)
	private String protocol;// pop3 ,imap
	@Size(max = 20)
	@Column(length = 20)
	private String username;
	@Size(max = 20)
	@Column(length = 20)
	private String password;
	private boolean inSsl;
	@Size(max = 20)
	@Column(length = 20)
	private String inHost;
	private int inPort;
	private boolean outSsl;
	@Size(max = 20)
	@Column(length = 20)
	private String outHost;
	private int outPort;

	@Column(name = "USER_ID")
	private Long userId;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "USER_ID", insertable = false, updatable = false)
	private User user;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isInSsl() {
		return inSsl;
	}

	public void setInSsl(boolean inSsl) {
		this.inSsl = inSsl;
	}

	public String getInHost() {
		return inHost;
	}

	public void setInHost(String inHost) {
		this.inHost = inHost;
	}

	public int getInPort() {
		return inPort;
	}

	public void setInPort(int inPort) {
		this.inPort = inPort;
	}

	public boolean isOutSsl() {
		return outSsl;
	}

	public void setOutSsl(boolean outSsl) {
		this.outSsl = outSsl;
	}

	public String getOutHost() {
		return outHost;
	}

	public void setOutHost(String outHost) {
		this.outHost = outHost;
	}

	public int getOutPort() {
		return outPort;
	}

	public void setOutPort(int outPort) {
		this.outPort = outPort;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
