package cn.ctodb.security;

import java.util.Collection;
import java.util.Vector;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.springframework.security.core.GrantedAuthority;

public class User extends org.springframework.security.core.userdetails.User implements HttpSessionBindingListener {

	private static final Vector<User> USERS = new Vector<User>();
	private static final Vector<HttpSession> SESSIONS = new Vector<HttpSession>();

	public User(Long userid, String username, String password, Collection<? extends GrantedAuthority> authorities) {
		super(username, password, authorities);
		this.userId = userid;
	}

	private static final long serialVersionUID = 1L;
	private Long userId;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public void valueBound(final HttpSessionBindingEvent arg0) {
		USERS.add(this);
		SESSIONS.add(arg0.getSession());
	}

	@Override
	public void valueUnbound(HttpSessionBindingEvent arg0) {
		USERS.remove(this);
		SESSIONS.remove(arg0.getSession());
	}

}
