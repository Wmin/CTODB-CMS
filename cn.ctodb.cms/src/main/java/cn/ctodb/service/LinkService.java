package cn.ctodb.service;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cn.ctodb.domain.Link;
import cn.ctodb.repository.LinkRepository;

@Service
public class LinkService {

    @Resource
    private LinkRepository linkRepository;

    public Page<Link> getLinks(Pageable pageable) {
        return linkRepository.findAll(pageable);
    }

}
