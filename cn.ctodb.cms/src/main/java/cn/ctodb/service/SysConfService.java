package cn.ctodb.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.ctodb.domain.SysConf;
import cn.ctodb.repository.SysConfRepository;

@Service
@Transactional
public class SysConfService {

    @Inject
    private SysConfRepository sysConfRepository;

    public SysConf getByCode(String code) {
        return sysConfRepository.findOneByCode(code);
    }

    public Map<String, String> getBaseConf() {
        Map<String, String> re = new HashMap<>();
        List<SysConf> list = sysConfRepository.findAllByType("base");
        for (SysConf sysConf : list) {
            re.put(sysConf.getCode(), sysConf.getValue());
        }
        return re;
    }

}
