package cn.ctodb.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import cn.ctodb.domain.ContentRes;
import cn.ctodb.repository.ContentResRepository;

@Service
public class ContentResService {

    @Resource
    private ContentResRepository contentResRepository;

    public void add(ContentRes contentRes) {
        contentResRepository.save(contentRes);
    }

}
