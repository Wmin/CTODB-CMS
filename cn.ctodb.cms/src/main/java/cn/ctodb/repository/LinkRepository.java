package cn.ctodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.ctodb.domain.Link;

public interface LinkRepository extends JpaRepository<Link, Long> {

}
