package cn.ctodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.ctodb.domain.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    public Category findByCode(String code);

}
