package cn.ctodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.ctodb.domain.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {

    Tag findByName(String tag);

}
