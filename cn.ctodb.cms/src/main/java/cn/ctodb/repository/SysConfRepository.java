package cn.ctodb.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.ctodb.domain.SysConf;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface SysConfRepository extends JpaRepository<SysConf, Long> {

	SysConf findOneByCode(String code);

	Optional<SysConf> findOneById(Long userId);

	@Override
	void delete(SysConf t);

	List<SysConf> findAllByType(String type);

}
