package cn.ctodb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.ctodb.app.mail.MailAccount;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface MailAccountRepository extends JpaRepository<MailAccount, Long> {

	MailAccount findOneByUserId(Long userId);

}
