package cn.ctodb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.ctodb.domain.Content;

public interface ContentRepository extends JpaRepository<Content, Long> {

    List<Content> findByCgId(long cgId);

    int countByTagsLike(String tag);

}
