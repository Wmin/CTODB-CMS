package cn.ctodb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.ctodb.domain.ContentRes;

public interface ContentResRepository extends JpaRepository<ContentRes, Long> {

}
