package cn.ctodb.action;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.ctodb.service.SysConfService;

@Controller
public class LoginAction {

	@Resource
	private SysConfService sysConfService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String index(ModelMap modelMap) {
		modelMap.put("base", sysConfService.getBaseConf());
		return "login";
	}

}
