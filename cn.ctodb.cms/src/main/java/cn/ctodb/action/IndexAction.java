package cn.ctodb.action;

import javax.annotation.Resource;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.ctodb.service.SysConfService;

@Controller
public class IndexAction {

	@Resource
	private SysConfService sysConfService;

	@Secured(value = { "ROLE_USER" })
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(ModelMap modelMap) {
		return "index";
	}

}
