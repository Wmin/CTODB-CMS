package cn.ctodb.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "sys_dic_item")
public class SysDicItem extends AbstractAuditingEntity {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long              id;

    @JsonIgnore
    @ManyToOne
    private SysDic            sysDic;

    @Size(max = 100)
    @Column(length = 100)
    private String            name;

    @Size(max = 20)
    @Column(length = 20)
    private String            code;

    @Size(max = 1)
    @Column(length = 1)
    private int               status;

    @Column(name = "item_order", length = 9)
    private int               itemOrder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SysDic getSysDic() {
        return sysDic;
    }

    public void setSysDic(SysDic sysDic) {
        this.sysDic = sysDic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(int itemOrder) {
        this.itemOrder = itemOrder;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
