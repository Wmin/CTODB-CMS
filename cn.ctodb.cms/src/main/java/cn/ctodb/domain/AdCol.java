//package cn.ctodb.domain;
//
//import javax.persistence.Entity;
//import javax.persistence.Table;
//
//import org.hibernate.annotations.Cache;
//import org.hibernate.annotations.CacheConcurrencyStrategy;
//
///**
// * 广告栏位
// * 
// * @author 超
// *
// */
//@Entity
//@Table(name = "cms_ad_col")
//@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
//public class AdCol {
//
//    private Long   id;
//    private String memo;
//    private String code;
//}
