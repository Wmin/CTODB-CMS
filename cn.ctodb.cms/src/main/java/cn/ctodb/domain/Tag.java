package cn.ctodb.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cms_tag")
public class Tag implements Serializable {

    private static final long serialVersionUID = 7615299815967242339L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long   id;
    private String name;
    @Column(name = "article_count")
    private long   articleCount;

    public long getArticleCount() {
        return articleCount;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setArticleCount(long articleCount) {
        this.articleCount = articleCount;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

}
