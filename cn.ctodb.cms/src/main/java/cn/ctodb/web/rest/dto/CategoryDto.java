package cn.ctodb.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import cn.ctodb.domain.Category;

@JsonInclude(Include.NON_NULL)
public class CategoryDto {

    private Long    id;
    private String  name;
    private String  code;
    private Long    pid;
    private String  status;
    private Long    articleCount;
    private Integer sort;
    private String  cgtp;
    private String  atp;
    private String  content;
    private String  memo;

    public CategoryDto() {

    }

    public CategoryDto(Category category) {
        this(category.getId(), category.getName(), category.getCode(), category.getPid(), category.getStatus(),
                category.getArticleCount(), category.getSort(), category.getCgtp(), category.getAtp(),
                category.getContent(), category.getMemo());
    }

    public CategoryDto(Long id, String name, String code, Long pid, String status, Long articleCount, Integer sort,
            String cgtp, String atp, String content, String memo) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.pid = pid;
        this.status = status;
        this.articleCount = articleCount;
        this.sort = sort;
        this.cgtp = cgtp;
        this.atp = atp;
        this.content = content;
        this.memo = memo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getArticleCount() {
        return articleCount;
    }

    public void setArticleCount(Long articleCount) {
        this.articleCount = articleCount;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getCgtp() {
        return cgtp;
    }

    public void setCgtp(String cgtp) {
        this.cgtp = cgtp;
    }

    public String getAtp() {
        return atp;
    }

    public void setAtp(String atp) {
        this.atp = atp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

}
