package cn.ctodb.web.rest.dto;

import java.util.List;

import javax.mail.Address;
import javax.mail.internet.InternetAddress;

public class MailDTO {

	private String id;
	private String subject;
	private address from;
	private String avatar;
	private String content;
	private String date;
	private String label;
	private String fold;
	private List<to> to;
	private List<attach> attach;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public address getFrom() {
		return from;
	}

	public void setFrom(address from) {
		this.from = from;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getFold() {
		return fold;
	}

	public void setFold(String fold) {
		this.fold = fold;
	}

	public List<to> getTo() {
		return to;
	}

	public void setTo(List<to> to) {
		this.to = to;
	}

	public List<attach> getAttach() {
		return attach;
	}

	public void setAttach(List<attach> attach) {
		this.attach = attach;
	}

	public class to {
		public String name;
		public String email;
	}

	public class attach {
		public String name;
		public String url;
	}

	public static class address {
		public String personal;
		public String address;

		public address() {
		}

		public address(Address address) {
			InternetAddress a = (InternetAddress) address;
			this.personal = a.getPersonal();
			this.address = a.getAddress();
		}

		public address(String personal, String address) {
			this.address = address;
			this.personal = personal;
		}
	}

}
