/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package cn.ctodb.web.rest.dto;
