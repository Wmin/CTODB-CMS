package cn.ctodb.web.rest.dto;

import cn.ctodb.config.Constants;

import cn.ctodb.domain.Authority;
import cn.ctodb.domain.User;

import org.hibernate.validator.constraints.Email;

import javax.validation.constraints.*;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A DTO representing a user, with his authorities.
 */
public class UserDTO {

	@NotNull
	@Pattern(regexp = Constants.LOGIN_REGEX)
	@Size(min = 1, max = 50)
	private String code;

	@Size(max = 50)
	private String name;

	@Email
	@Size(min = 5, max = 100)
	private String email;

	@Size(min = 5, max = 20)
	private String tel;

	private boolean activated = false;

	@Size(min = 2, max = 5)
	private String langKey;

	private Set<String> authorities;

	public UserDTO() {
	}

	public UserDTO(User user) {
		this(user.getCode(), user.getName(), user.getEmail(), user.getTel(), user.getActivated(), user.getLangKey(),
				null);
		//this.authorities = user.getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet());
	}

	public UserDTO(String code, String name, String email, String tel, boolean activated, String langKey,
			Set<String> authorities) {
		this.code = code;
		this.name = name;
		this.email = email;
		this.tel = tel;
		this.activated = activated;
		this.langKey = langKey;
		this.authorities = authorities;
	}

	public String getEmail() {
		return email;
	}

	public boolean isActivated() {
		return activated;
	}

	public String getLangKey() {
		return langKey;
	}

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Set<String> getAuthorities() {
		return authorities;
	}

	@Override
	public String toString() {
		return "UserDTO{" + "login='" + code + '\'' + ", name='" + name + '\'' + ", email='" + email + '\''
				+ ", activated=" + activated + ", langKey='" + langKey + '\'' + ", authorities=" + authorities + "}";
	}
}
