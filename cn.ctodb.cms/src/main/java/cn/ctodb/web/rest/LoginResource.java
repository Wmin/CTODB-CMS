package cn.ctodb.web.rest;

import com.codahale.metrics.annotation.Timed;

import cn.ctodb.domain.PersistentToken;
import cn.ctodb.domain.User;
import cn.ctodb.repository.PersistentTokenRepository;
import cn.ctodb.repository.UserRepository;
import cn.ctodb.security.SecurityUtils;
import cn.ctodb.service.SysMailService;
import cn.ctodb.service.UserService;
import cn.ctodb.web.rest.dto.KeyAndPasswordDTO;
import cn.ctodb.web.rest.dto.ManagedUserDTO;
import cn.ctodb.web.rest.dto.UserDTO;
import cn.ctodb.web.rest.util.HeaderUtil;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * REST controller for managing the current user's account.
 */
@RestController
@RequestMapping("/api")
public class LoginResource {

    private final Logger              log = LoggerFactory.getLogger(LoginResource.class);

    @Inject
    private UserRepository            userRepository;

    @Inject
    private UserService               userService;

    @Inject
    private PersistentTokenRepository persistentTokenRepository;

    @Inject
    private SysMailService               mailService;

    /**
     * POST /register : register the user.
     *
     * @param managedUserDTO
     *            the managed user DTO
     * @param request
     *            the HTTP request
     * @return the ResponseEntity with status 201 (Created) if the user is
     *         registred or 400 (Bad Request) if the login or e-mail is already
     *         in use
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE })
    @Timed
    public ResponseEntity<?> login(@Valid @RequestBody ManagedUserDTO managedUserDTO, HttpServletRequest request) {

        HttpHeaders textPlainHeaders = new HttpHeaders();
        textPlainHeaders.setContentType(MediaType.TEXT_PLAIN);

        return null;
    }

}
