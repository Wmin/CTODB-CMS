package cn.ctodb.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import cn.ctodb.domain.Category;
import cn.ctodb.service.CategoryService;
import cn.ctodb.web.rest.dto.AbnTreeDto;
import cn.ctodb.web.rest.dto.CategoryDto;

@RestController
@RequestMapping("/api/category")
public class CategoryResource {

    @Inject
    private CategoryService categoryService;

    @Timed
    @RequestMapping(value = "/tree", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public List<AbnTreeDto> tree() {
        return categoryService.getTreeDto();
    }

    @Timed
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public AbnTreeDto add(@RequestBody Category category) {
        categoryService.add(category);
        return categoryService.getDto(category);
    }

    @Timed
    @RequestMapping(value = "/get", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public CategoryDto get(@RequestBody CategoryDto categoryDto) {
        return new CategoryDto(categoryService.get(categoryDto.getId()));
    }

    @Timed
    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
    public CategoryDto save(@RequestBody CategoryDto categoryDto) {
        categoryService.up(categoryDto);
        return categoryDto;
    }

}
