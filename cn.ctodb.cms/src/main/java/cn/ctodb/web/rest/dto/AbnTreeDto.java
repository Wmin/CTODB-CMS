package cn.ctodb.web.rest.dto;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class AbnTreeDto {

    private Long            id;
    private String          label;
    private Set<AbnTreeDto> children;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Set<AbnTreeDto> getChildren() {
        return children;
    }

    public void setChildren(Set<AbnTreeDto> children) {
        this.children = children;
    }

}
