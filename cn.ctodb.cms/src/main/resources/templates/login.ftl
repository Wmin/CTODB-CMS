<!DOCTYPE html>
<html lang="zh">
<head>
<title>${base['ws.name']}</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome.min.css" type="text/css" />
<link href='http://fonts.useso.com/css?family=Open+Sans:400,700,800' />
<link rel="stylesheet" href="css/templatemo_style.css" />
<script src="vendor/jquery/jquery.min.js"></script>
<script src="//cdn.bootcss.com/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
</head>
<body class="templatemo-bg-gray">
    <div class="container">
        <div class="col-md-12">
            <h1 class="margin-bottom-15">Login To ${base['ws.name']}</h1>
            <form class="form-horizontal templatemo-container templatemo-login-form-1 margin-bottom-30" role="form">
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="control-wrapper">
                            <label for="username" class="control-label fa-label"><i class="fa fa-user fa-medium"></i></label>
                            <input name="code" type="text" class="form-control" placeholder="Username">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="control-wrapper">
                            <label for="password" class="control-label fa-label"><i class="fa fa-lock fa-medium"></i></label>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="checkbox control-wrapper">
                            <label> <input id="remember" type="checkbox"> Remember me
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <div class="control-wrapper">
                            <input type="button" onclick="login()" value="Log in" class="btn btn-info">
                            <!-- <a href="forgot-password.html" class="text-right pull-right">Forgot password?</a> -->
                        </div>
                    </div>
                </div>
                <hr>
                <div class="form-group">
                    <div class="text-right">${base['ws.copyright']}</div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
		function login() {
			var code = $("[name='code']").val();
			var password = $("[name='password']").val();
			jQuery.ajax({
				url : "api/authentication",
				data : {
					code : code,
					password : password,
					'remember-me' : $('#remember').is(":checked"),
					'submit' : 'Login'
				},
				headers : {
					"X-CSRF-TOKEN" : $.cookie("CSRF-TOKEN")
				},
				type : "POST",
				success : function() {
					window.location.href = '/';
				}
			});
		}
	</script>
</body>
</html>
