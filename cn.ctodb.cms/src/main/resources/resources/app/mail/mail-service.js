// A RESTful factory for retreiving mails from 'mails.json'
app.factory('mails', [ '$http', function($http) {

	var factory = {};
	factory.list = function(folder,page) {
		return $http.get('api/mail/inbox/' + folder+"?page="+page).then(function(resp) {
			return resp.data;
		});
	};
	factory.folders = function() {
		return $http.get("api/mail/folders").then(function(resp) {
			return resp.data;
		});
	};
	factory.get = function(id) {
		return mails.then(function(mails) {
			for (var i = 0; i < mails.length; i++) {
				if (mails[i].id == id)
					return mails[i];
			}
			return null;
		})
	};
	return factory;
} ]);