//(function() {
//    'use strict';
//
//    angular
//        .module('app')
//        .config(stateConfig);
//
//    stateConfig.$inject = ['$stateProvider'];
//
//    function stateConfig($stateProvider) {
//    	
//        $stateProvider.state('access', {
//            url: '/access',
//            template: '<div ui-view class="fade-in-right-big smooth"></div>'
//        })
//        .state('access.login', {
//            url: '/login',
//            templateUrl: 'app/components/login/login.html',
//            resolve: {
//                deps: ['uiLoad',
//                  function( uiLoad ){
//                    return uiLoad.load( ['app/components/login/login.controller.js'] );
//                }]
//            }
//        })
//        .state('access.register', {
//            url: '/register',
//            templateUrl: 'app/account/register/register.html',
//            resolve: {
//                deps: ['uiLoad',
//                  function( uiLoad ){
//                    return uiLoad.load( ['app/account/register/register.controller.js'] );
//                }]
//            }
//        })
//        .state('access.forgotpwd', {
//            url: '/forgotpwd',
//            templateUrl: 'tpl/page_forgotpwd.html'
//        });
//    }
//})();
