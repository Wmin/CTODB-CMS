/* Controllers */
// login controller
app.controller('signinFormController', 
		['$scope','$http','$state','$rootScope','AuthServerProvider',
		function($scope, $http, $state, $rootScope,AuthServerProvider) {
			if ($rootScope.user) {
				alert("was login");
				//$state.go("app.dashboard");
				return;
			}
			$scope.user = {code:'admin',password:"123456",rememberMe:true};
			$scope.authError = null;
			$scope.login = function() {
				$scope.authError = null;
				$scope.user.rememberMe=true;
				AuthServerProvider.login($scope.user).then(function(){
					$rootScope.$broadcast('authenticationSuccess');
                 	$state.go('app.dashboard');
                 }).catch(function(e){
                 	$scope.authError = '登录失败:'+e.data.message;
  	       	    });
			};
		} ]);