'use strict';

/* Controllers */

angular.module('app')
  .controller('CtodbController', 
		  ['$scope', '$translate', '$localStorage', '$window', '$rootScope','$state','Account','AuthServerProvider','$timeout','$modal',
    function($scope,   $translate,   $localStorage,   $window ,$rootScope,$state,Account,AuthServerProvider,$timeout,$modal) {
	  $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
		  if($rootScope.user)return;
      	  Account.get().$promise.then(function(account){
      		  $rootScope.user = account.data;
      	  }).catch(function(){
      		  window.location.href = 'login';
      	  });
      });
      
      // config
      $scope.app = {
    		  name: 'CTODB.CN',
    		  version: '1.3.3',
    		  Copyright : '© 2016 Copyright',
    		  // for chart colors
    		  color: {
    			  primary: '#7266ba',
    			  info:    '#23b7e5',
    			  success: '#27c24c',
    			  warning: '#fad733',
    			  danger:  '#f05050',
    			  light:   '#e8eff0',
    			  dark:    '#3a3f51',
    			  black:   '#1c2b36'
    		  },
    		  settings: {
    			  themeID: 1,
    			  navbarHeaderColor: 'bg-black',
    			  navbarCollapseColor: 'bg-white-only',
    			  asideColor: 'bg-black',
    			  headerFixed: true,
    			  asideFixed: false,
    			  asideFolded: false,
    			  asideDock: false,
    			  container: false
    		  }
      }
      // angular translate
      $scope.lang = { isopen: false };
      $scope.langs = {cn:'中文', en:'English', de_DE:'German', it_IT:'Italian'};
      $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "中文";
      
	  // add 'ie' classes to html
      var isIE = !!navigator.userAgent.match(/MSIE/i);
      isIE && angular.element($window.document.body).addClass('ie');
      isSmartDevice( $window ) && angular.element($window.document.body).addClass('smart');

      // save settings to local storage
      if ( angular.isDefined($localStorage.settings) ) {
        $scope.app.settings = $localStorage.settings;
      } else {
        $localStorage.settings = $scope.app.settings;
      }
      
      
      
      
      $scope.setLang = function(langKey, $event) {
	      // set the current lang
	      $scope.selectLang = $scope.langs[langKey];
	      // You can change the language during runtime
	      $translate.use(langKey);
	      $scope.lang.isopen = !$scope.lang.isopen;
	  };
        
      $scope.$watch('app.settings', function(){
        if( $scope.app.settings.asideDock  &&  $scope.app.settings.asideFixed ){
          // aside dock and fixed must set the header fixed.
          $scope.app.settings.headerFixed = true;
        }
        // save to local storage
        $localStorage.settings = $scope.app.settings;
      }, true);

     
      
      function isSmartDevice( $window )
      {
          // Adapted from http://www.detectmobilebrowsers.com
          var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
          // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
          return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
      }
      $scope.logout = function logout() {
			AuthServerProvider.logout().then(function() {
//				$state.go("access.signin");
				window.location.href = '/';
			});
		}
		$rootScope.open = function(option, success, error) {
			var modalInstance = $modal.open(option);
			modalInstance.result.then(success, error);
		}
		$scope.pwdUp = function() {
			var option = {
				templateUrl : 'app/sys/pwdup.html',
				controller : 'PwdUpController',
				// size : 100,
				resolve : {
					deps : [ '$ocLazyLoad', function($ocLazyLoad) {
						return $ocLazyLoad.load([ 'app/sys/pwdup.js' ]);
					} ]
				}
			};
			$rootScope.open(option, function() {
			}, function() {
			});
		};
  }]);