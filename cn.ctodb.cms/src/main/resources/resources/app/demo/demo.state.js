(function() {
    'use strict';
    app.config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('app.demo', {
            url: '/demo',
            templateUrl: 'app/demo/demo.html',
        });
    }
})();
