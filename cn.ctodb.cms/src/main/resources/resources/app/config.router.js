(function() {
	'use strict';
	angular.module('app').config(stateConfig);
	stateConfig.$inject = [ '$stateProvider', '$urlRouterProvider' ];
	function stateConfig($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/dashboard');
		$stateProvider
				.state('app', {
					abstract : true,
					url : '',
					templateUrl : 'app/sys/app.html'
				})
				.state('app.dashboard', {
					url : '/dashboard',
					templateUrl : 'app/dashboard/app_dashboard.html'
				})
				.state('app.myinfo', {
					url : '/myinfo',
					templateUrl : 'tpl/myinfo/myinfo.html'
				})
				.state('app.myinfo.profile', {
					url : '/profile',
					templateUrl : 'tpl/profile.html'
				})
				.state('app.docs', {
					url : '/docs',
					templateUrl : 'tpl/docs.html'
				})
				// others
				.state('access',{
					url : '/access',
					template : '<div ui-view class="fade-in-right-big smooth"></div>'
				})
				.state('access.404', {
					url : '/404',
					templateUrl : 'tpl/page_404.html'
				})
	            .state('app.category', {
	                  url: '/category',
	                  templateUrl: 'app/category/category.html',
	                  resolve: {
	                      deps: ['$ocLazyLoad',
	                        function( $ocLazyLoad ){
	                          return $ocLazyLoad.load('angularBootstrapNavTree').then(
	                              function(){
	                                 return $ocLazyLoad.load('app/category/category.js');
	                              }
	                          );
	                        }
	                      ]
	                  }
	              })
				.state('apps', {
					abstract : true,
					url : '/apps',
					templateUrl : 'tpl/layout.html'
				})
				.state(
						'apps.contact',
						{
							url : '/contact',
							templateUrl : 'tpl/apps_contact.html',
							resolve : {
								deps : [
										'uiLoad',
										function(uiLoad) {
											return uiLoad
													.load([ 'js/app/contact/contact.js' ]);
										} ]
							}
						})
	}
})();
