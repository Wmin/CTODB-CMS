'use strict';

var app = angular
	.module( 'app',
		[ 'ngAnimate', 
		  'ngCookies', 
		  'ngResource', 
		  'ngSanitize',
		  'ngTouch', 
		  'ngStorage', 
		  'ui.router', 
		  'ui.bootstrap',
		  'ui.load', 
		  'ui.jq', 
		  'ui.validate', 
		  'oc.lazyLoad',
		  'pascalprecht.translate' 
		 ]
	).config(
		 ['$urlRouterProvider',
		  '$httpProvider',
			// 'httpRequestInterceptorCacheBusterProvider',
		  '$urlMatcherFactoryProvider',
		  function httpConfig($urlRouterProvider, $httpProvider,
					// httpRequestInterceptorCacheBusterProvider,
					$urlMatcherFactoryProvider) {
			 	// enable CSRF
				$httpProvider.defaults.xsrfCookieName = 'CSRF-TOKEN';
				$httpProvider.defaults.xsrfHeaderName = 'X-CSRF-TOKEN';
	
				// Cache everything except rest api requests
				// httpRequestInterceptorCacheBusterProvider.setMatchlist([
				// /.*api.*/, /.*protected.*/ ], true);
	
				$urlRouterProvider.otherwise('/');
	
				// $httpProvider.interceptors.push('errorHandlerInterceptor');
				// $httpProvider.interceptors.push('authExpiredInterceptor');
				// $httpProvider.interceptors.push('notificationInterceptor');
				// jhipster-needle-angularjs-add-interceptor
				// JHipster will
				// add new
				// application http interceptor here
	
				$urlMatcherFactoryProvider.type('boolean',
						{
							name : 'boolean',
							decode : function(val) {
								return val === true
										|| val === 'true';
							},
							encode : function(val) {
								return val ? 1 : 0;
							},
							equals : function(a, b) {
								return this.is(a) && a === b;
							},
							is : function(val) {
								return [ true, false, 0, 1 ]
										.indexOf(val) >= 0;
							},
							pattern : /bool|true|0|1/
						});
			} 
		]
	).config(
		    ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
	            function ($controllerProvider,   $compileProvider,   $filterProvider,   $provide) {
	                 // lazy controller, directive and service
	                 app.controller = $controllerProvider.register;
	                 app.directive  = $compileProvider.directive;
	                 app.filter     = $filterProvider.register;
	                 app.factory    = $provide.factory;
	                 app.service    = $provide.service;
	                 app.constant   = $provide.constant;
	                 app.value      = $provide.value;
	            }
		      ]
	).config(
			['$translateProvider', 
			  function($translateProvider){
		             // Register a loader for the static files
		             // So, the module will search missing translation tables under the specified urls.
		             // Those urls are [prefix][langKey][suffix].
		             $translateProvider.useStaticFilesLoader({
		               prefix: 'l10n/',
		               suffix: '.js'
		             });
		             // Tell the module what language to use by default
		             $translateProvider.preferredLanguage('en');
		             // Tell the module to store the language in the local storage
		             $translateProvider.useLocalStorage();
				}
			]
	).run(
		['$rootScope', '$state', '$stateParams','Account',
           function ($rootScope,   $state,   $stateParams ,Account) {
			
               $rootScope.$state = $state;
               $rootScope.$stateParams = $stateParams;

               //url监听
               $rootScope.$on('$stateChangeStart',function(event, toState, toParams, fromState, fromParams){
             	  if($rootScope.user)return;
             	  console.log(toState);
             	  if(toState.name=='access.login'
             		  ||toState.name=='access.register'
             		  ||toState.name=='access.forgotpwd'){
             		  return;
             		  
             	  }
       	      	  Account.get().$promise.then(function(account){
       	      		  $rootScope.user = account.data;
       	      		  if(toState.name=='access.signin'){
       	      			  $state.go("app.dashboard");//跳转到首页
       	      		  }
       	      	  }).catch(function(){
       	      		  if(toState.name!='access.login') {
       	      			  event.preventDefault();// 取消默认跳转行为
       	      			  $state.go("access.login",{from:fromState.name,w:'notLogin'});//跳转到登录界面
       	      		  }
       	      	  });
       	      });
           }
        ]
     );