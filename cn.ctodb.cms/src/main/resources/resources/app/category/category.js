(function() {
	'use strict';
	app.controller('CategoryController',
			[ '$scope', '$http', '$timeout', func ]);
	function func($scope, $http, $timeout) {
		var apple_selected, tree, treedata_avm;
		$scope.category = {};
		$scope.info = function(branch) {
			var data = {
				id : branch.id
			};
			$http.post('api/category/get', data).success(function(response) {
				$scope.category = response;
			});
		};
		$scope.my_tree = tree = {};
		$scope.my_data = [];
		$scope.load = function() {
			$scope.my_data = [];
			$scope.doing_async = true;
			$http.post('api/category/tree').success(function(response) {
				$scope.my_data = response;
				$scope.doing_async = false;
				return $timeout(function() {
					return tree.expand_all();
				}, 100);
			});
		};
		$scope.add = function() {
			var b = tree.get_selected_branch();
			var data = {
				pid : b.id,
				name : "new"
			};
			$http.post('api/category/add', data).success(function(response) {
				return tree.add_branch(b, response);
			});
		}
		$scope.save = function() {
			$http.post('api/category/save', $scope.category).success(
					function(response) {
						$scope.category = response;
					});
		}
		$scope.load();
	}
})();