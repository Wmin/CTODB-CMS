(function() {
	'use strict';
	app.controller('PwdUpController', [ '$scope', '$modalInstance', 'AuthServerProvider', func ]);
	function func($scope, $modalInstance, AuthServerProvider) {
		$scope.pwdup = function() {
			AuthServerProvider.pwdup($scope.srcpwd,$scope.newpwd).then(function() {
				$modalInstance.close();
			}).catch(function(e){
				console.info(e);
            	$scope.authError = '操作失败:'+e.data.message;
  	      	});
		}
	}
})();
